#!/bin/bash

# error handling
set -eu -o pipefail
shopt -s failglob inherit_errexit

# check if root

[[ $EUID -ne 0 ]] && { echo "Must be run as root"; exit 1; }

apt update && sudo apt install -y tiger
cp -v zabbix_agentd.d/tiger-alerts.conf /etc/zabbix/zabbix_agentd.conf.d/
cp -v tiger-alerts /usr/local/bin
chmod -v +x /usr/local/bin/tiger-alerts

echo "In Zabbix create a new Item with Key tiger-alerts"
