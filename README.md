# zabbix-tiger

Zabbix integration for tiger system security vulnerabilities reporting.

The script locally runs `tiger` and counts the number of alerts.
These are reported to zabbix.

## Installation

Run the install script `install.sh` or the following commands as root:

    apt update && sudo apt install -y tiger
    cp -v zabbix_agentd.d/tiger-alerts.conf /etc/zabbix/zabbix_agentd.conf.d/
    cp -v tiger-alerts /usr/local/bin
    chmod -v +x /usr/local/bin/tiger-alerts

Then in zabbix create a new application "Tiger" and an item with Key: "tiger-alerts".
